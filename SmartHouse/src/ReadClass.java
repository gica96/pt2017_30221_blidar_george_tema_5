import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReadClass {
	
	private List<MonitoredData> monitoredData;
	private final String filename = "D:\\Proiecte Java\\SmartHouse\\Activities.txt";
	
	public ReadClass()
	{
		monitoredData = new ArrayList<MonitoredData>();
	}
	
	public List<MonitoredData> getData()
	{
		return this.monitoredData;
	}
	
	public void readData()
	{
		BufferedReader br = null;
		FileReader fr = null;
		try 
		{
			fr = new FileReader(filename);
			br = new BufferedReader(fr);

			String sCurrentLine;
			String [] tokens;

			br = new BufferedReader(new FileReader(filename));

			while ((sCurrentLine = br.readLine()) != null) 
			{
				tokens = sCurrentLine.split("		");
				MonitoredData data = new MonitoredData(tokens[0],tokens[1],tokens[2]);
				monitoredData.add(data);
			}

		} catch (IOException e) 
		{
			e.printStackTrace();
		} 
		finally 
		{
			try 
			{

				if (br != null)
					br.close();

				if (fr != null)
					fr.close();

			}
			catch (IOException ex) 
			{
				ex.printStackTrace();
			}
		}
	}
}