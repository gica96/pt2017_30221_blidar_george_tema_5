import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class TestClass {
	
	public void firstTask() // citirea din fisier - prima cerinta
	{
		ReadClass reader = new ReadClass();
		reader.readData();
		List<MonitoredData> data = reader.getData();
		System.out.println("Datele citite din fisierul Activities.txt sunt:\n");
		for(MonitoredData m : data)
		{
			System.out.println(m.getStartTime() + "  " + m.getEndTime() + "  " + m.getActivity());
		}
		System.out.println();
	}
	
	public void secondTask() // numararea zilelor distincte - a doua cerinta
	{
		
		ReadClass reader = new ReadClass();
		reader.readData();
		List<MonitoredData> data = reader.getData();
		
		System.out.print("Numarul zilelor distincte este: ");
		
		long number =	data.stream()
				.map(MonitoredData::getStartDate)
			   //.filter(start -> start.areDifferent())
			   .distinct()
			   .count(); // daca se alege varianta cu filter => mai punem un +1 la final;
		
		System.out.print(number);
		System.out.println();
		System.out.println();
	}
	
	public void thirdTask() // maparea activitatilor intr-un HashMap de de tip <String,Integer>, unde valoarea repr. nr. de aparitii a activitatii
	{
		ReadClass reader = new ReadClass();
		reader.readData();
		List<MonitoredData> data = reader.getData();
		
		List<String> activities = new ArrayList<String>();
		Map<String, Integer> occurrencesOfActivities = new HashMap<String, Integer>();
		
		activities = data.stream().map(MonitoredData::getActivity).collect(Collectors.toList());
		
		//for(String s : activities)
		//{
			activities.forEach((s) ->occurrencesOfActivities.putIfAbsent(s, 0));
		//}
		
		//for(String s : activities)
		//{
			activities.forEach((s) ->occurrencesOfActivities.computeIfPresent(s, (key,value) -> value + 1));
		//}
		occurrencesOfActivities.forEach((k,v)->System.out.println("Activitatea: " + k + " | cu numar de aparitii: " + v));
		System.out.println();
	}
	
	public void fourthTask() // cerinta anterioara aplicata pentru fiecare zi in parte
	{
		ReadClass reader = new ReadClass();
		reader.readData();
		
		List<MonitoredData> data = reader.getData();
		List<String> startingDates = new ArrayList<String>();
		Map<Integer, Map<String, Integer>> occurrencesOfActivitiesForEachDay = new HashMap<Integer, Map<String, Integer>>();

		startingDates = data.stream().map(MonitoredData::getStartDate).distinct().collect(Collectors.toList());
		//final int dayNumber;
		
		AtomicInteger counter = new AtomicInteger(1);

		//for(String s : startingDates)
		//{
			startingDates.forEach((s) -> {
			List<MonitoredData> filteredData= data.stream().filter((d)->d.getActivityByDate(s)).collect(Collectors.toList());
			List<String> filteredActivities = filteredData.stream().map(MonitoredData::getActivity).collect(Collectors.toList());
			Map<String, Integer> occurrencesOfActivities = new HashMap<String, Integer>();

			//for(String a : filteredActivities)
			//{
				filteredActivities.forEach((a) -> occurrencesOfActivities.putIfAbsent(a, 0));
			//}
			//for(String a : filteredActivities)
			//{
				filteredActivities.forEach((a) -> occurrencesOfActivities.computeIfPresent(a, (k,v) -> v + 1));
			//}
			occurrencesOfActivitiesForEachDay.putIfAbsent(counter.get(), occurrencesOfActivities);
			counter.incrementAndGet();
			});
		//}
		occurrencesOfActivitiesForEachDay.forEach((k,v)-> { 
			System.out.println();
			System.out.println("Ziua: " + k + " contine activitatile impreuna cu numarul de aparitii ale acestora:");
			v.forEach((key,value) -> {
				System.out.println(key + " " + value);
			});
		});
	}
	
	public void fifthTask() // maparea activitatilor impreuna cu timpul total de desfasurare a acestora (Map<String, DateTime>)
	{                               // filtrarea activitatilor cu o durata mai mare de 10 ore
		
		ReadClass reader = new ReadClass();
		reader.readData();
		List<MonitoredData> data = reader.getData();
		List<String> activities = new ArrayList<String>();
		Map<String, String> activityDuration = new HashMap<String, String>();
		
		activities =data.stream().map(MonitoredData::getActivity).distinct().collect(Collectors.toList());
		
		//for(String s : activities)
		//{
			activities.forEach((s) -> {
			List<MonitoredData> filteredData = data.stream().filter((d) -> d.getDataByActivity(s)).collect(Collectors.toList());
			List<String> hours = filteredData.stream().map(MonitoredData::getHoursTogether).collect(Collectors.toList());
			AtomicInteger totalTime = new AtomicInteger(0);
			//for(String h : hours)
			//{
				hours.forEach((h) -> {
				String [] token = h.split(" ");
				SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
				Date date1 = null;
				Date date2 = null;
				try
				{
					 date1 = format.parse(token[0]);
					 date2 = format.parse(token[1]);
				}
				catch(ParseException ex)
				{
					ex.printStackTrace();
				}
				int result = Integer.parseInt(String.valueOf(date2.getTime()-date1.getTime()));
				totalTime.addAndGet(result);
				});
			//}
			if(totalTime.get() < 0)
			{
				totalTime.set(Math.abs(totalTime.get()));
			}
			String time = String.format("%02d:%02d:%02d", 
					TimeUnit.MILLISECONDS.toHours(totalTime.get()),
					TimeUnit.MILLISECONDS.toMinutes(totalTime.get()) -  
					TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(totalTime.get())), 
					TimeUnit.MILLISECONDS.toSeconds(totalTime.get()) - 
					TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalTime.get())));  
			activityDuration.putIfAbsent(s, time);
		//}
			});
		activityDuration.forEach((k,v) -> {
			System.out.println("\nPentru activitatea: " + k + " avem timpul: " + v);
		});
		System.out.println("\nActivitatile cu durata mai mare de 10 ore sunt: ");
		activityDuration.entrySet().stream().filter((d) -> d.getValue().compareTo("10:00:00") > 0).forEach((v) -> {
			System.out.println(v.getKey() + " " + v.getValue());
		});
		System.out.println();
	}
	
	public void sixthTask() // selectarea activitatilor care au in 90 la suta din cazuri durata de sub 5 minute
	{
		ReadClass reader = new ReadClass();
		reader.readData();
		List<MonitoredData> data = reader.getData();
		List<String> finalList = new ArrayList<String>();
		List<String> activities = new ArrayList<String>();
		System.out.println("Match-urile intre cate activitati au durata sub 5 minute si peste cate ar trebui sa fie ca sa fie valide pentru cerinta 5:");
		activities =data.stream().map(MonitoredData::getActivity).distinct().collect(Collectors.toList());
	//	for(String s : activities)
		//{
			activities.forEach((s) -> {
			List<MonitoredData> filteredData = data.stream().filter((d) -> d.getDataByActivity(s)).collect(Collectors.toList());
			List<String> hours = filteredData.stream().map(MonitoredData::getHoursTogether).collect(Collectors.toList());
			long count = filteredData.stream().count(); // numarul de aparitii ale activitatii "s"
			
			float num = (90.0f / 100.0f) * count;
			AtomicInteger checkValue = new AtomicInteger(0);
		//	for(String h : hours)
			//{
				hours.forEach((h) -> {
				String [] token = h.split(" ");
				AtomicInteger totalTime = new AtomicInteger(0);
				SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
				Date date1 = null;
				Date date2 = null;
				try
				{
					 date1 = format.parse(token[0]);
					 date2 = format.parse(token[1]);
				}
				catch(ParseException ex)
				{
					ex.printStackTrace();
				}
				totalTime.addAndGet((int)(date2.getTime() - date1.getTime()));
				if(totalTime.get() < 0)
				{
					totalTime.set(Math.abs(totalTime.get()));
				}
				String time = String.format("%02d:%02d:%02d", 
						TimeUnit.MILLISECONDS.toHours(totalTime.get()),
						TimeUnit.MILLISECONDS.toMinutes(totalTime.get()) -  
						TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(totalTime.get())), 
						TimeUnit.MILLISECONDS.toSeconds(totalTime.get()) - 
						TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalTime.get())));  
				if(time.compareTo("00:05:00") < 0)
				{
					checkValue.incrementAndGet();
				}
				});
			//}
			System.out.println("Pentru activitatea: " + s + " avem: " + " checkValue si numarul minim: " + checkValue.get() + " " + num);
			if(checkValue.get() > num)
			{
				finalList.add(s);
			}
		//}
			});
		System.out.println("Urmatoarele activitati satisfac conditia:");
		finalList.forEach(System.out::println);
	}
}
