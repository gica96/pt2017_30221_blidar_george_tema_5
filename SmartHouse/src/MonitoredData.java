
public class MonitoredData {
	
	private String startTime;
	private String endTime;
	private String activityLabel;
		
	public MonitoredData(String start, String end, String activity)
	{
		this.startTime = start;
		this.endTime = end;
		this.activityLabel = activity;
	}
		
	public String getStartTime()
	{
		return this.startTime;
	}
		
	public String getEndTime()
	{
		return this.endTime;
	}
		
	public String getActivity()
	{
		return this.activityLabel;
	}
		
	public String getStartDate()
	{
		String [] token;
		token = this.startTime.split(" ");
		return token[0];
	}
		
		public String getEndDate()
		{
			String [] token;
			token = this.endTime.split(" ");
			return token[0];
		}
		
		public String getStartHour()
		{
			String [] token;
			token = this.startTime.split(" ");
			return token[1];
		}
		
		public String getEndHour()
		{
			String [] token;
			token = this.endTime.split(" ");
			return token[1];
		}
		
		public String getHoursTogether()
		{
			String finalHour = getStartHour() + " " + getEndHour();
			return finalHour;
		}
		
		public boolean areDifferent()
		{
			if(this.getStartDate().compareTo(this.getEndDate()) != 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public boolean getActivityByDate(String date)
		{
			String [] tokens = this.startTime.split(" ");
			if(date.compareTo(tokens[0]) == 0)
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
		
		public boolean getDataByActivity(String activity)
		{
			if(activity.compareTo(this.activityLabel) == 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
}
